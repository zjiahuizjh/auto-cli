#! /usr/bin/env node

const program = require("commander");
const exec = require("child_process").exec;
const pkg = require("../package.json");
// const main = require("../lib/main");

console.log("if you used nodejs place choice -e,if you used typescript place choice -t")

program
  .version(pkg.version)//定义版本号
  .option('-e, --eslint', 'install eslint')//参数定义
  .option('-t, --pineapple', 'install tslint')
  .parse(process.argv);//解析命令行参数,参数定义完成后才能调用

// 获取用户输入指令
let choice = process.argv[2].slice(1)
console.log(choice)

// 如果我用的是if...else怎么去另一个文件分流
/*
if (choice === "e"){
    main.choice_eslint;
}else{
    main.choice_tslint;
}
*/
if (choice === "e"){
    // 这是测试命令，正式需要换成eslint
    exec("uname -a",function(error, stdout, stderr){
        if(error){
            console.log(error);
            return;
        }
        console.log("stdout" + stdout);
        console.log("stderr" + typeof stderr);
    })
}else{
    exec("ls -a",function(error, stdout, stderr){
        if(error){
            console.log(error);
            return;
        }
        console.log("stdout" + stdout);
        console.log("stderr" + typeof stderr);
    })
}